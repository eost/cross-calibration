#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from matplotlib import pyplot
import numpy as np
import pandas
from scipy.optimize import curve_fit
from scipy.signal import freqs_zpk


def _is_polarity_inverted(f, pha):
    """
    Tests polarity inversion according to the phase shift
    """
    fmin = np.percentile(f, 25)
    fmax = np.percentile(f, 75)
    imin = np.abs(f-fmin).argmin()
    imax = np.abs(f-fmax).argmin()
    if np.median(pha[imin:imax]) < -10:
        return True
    elif np.median(pha[imin:imax]) > 10:
        return False


def _calc_norm_factor(zeros, poles, fnorm):
    """
    Computes the normalization factor from the zeros, the poles and the
    normalization frequency.
    """
    return np.abs(np.prod(2j*np.pi*fnorm-poles) /
                  np.prod(2j*np.pi*fnorm-zeros))


def _calc_pz(fc, h):
    """
    Calculate poles and zeros from a cutoff frequency and a damping. Unlike
    corn_freq_2_paz from obspy, it returns a result if damping > 1.
    """
    w0 = 2*np.pi*fc
    zeros = np.array([0, 0])
    if h < 1:
        p1 = -1*h*w0 - 1j*w0*np.sqrt(1-h**2)
        p2 = -1*h*w0 + 1j*w0*np.sqrt(1-h**2)
    elif h >= 1:
        p1 = -1*h*w0 - w0*np.sqrt(h**2-1)
        p2 = -1*h*w0 + w0*np.sqrt(h**2-1)
    poles = np.array([p1, p2])
    return (zeros, poles)


def _model(f, fn, fc, h):
    """
    Computes the frequency transfer function from 2 poles (conjugated complex,
    real part and imaginary part) according to the model of a 2nd order
    low-pass filter.
    Zeros are hardly coded to [0, 0].
    Parameters are frequencies array and a normalization frequency.
    params = [f, fn]
    """
    (zeros, poles) = _calc_pz(fc, h)
    k = _calc_norm_factor(zeros, poles, fn)
    _, H = freqs_zpk(zeros, poles, k, np.multiply(2*np.pi, f))
    return H


def _model_pha(params, fc, h):
    """
    Computes the phase (in degrees) of a transfer function from 2 poles
    (conjugated complex, real part and imaginary part) using _model function
    (2nd order low-pass filter).
    Parameters are frequencies array and a normalization frequency, flattened
    in an array (params = np.append(f, fn)).
    """
    return np.angle(_model(params[0:-1], params[-1], fc, h), deg=True)


def _model_amp(params, k):
    """
    Computes the amplitude (in dB) of a transfer function from a sensitivity
    using _model function (2nd order low-pass filter).
    Parameters are frequencies array, a normalization frequency, poles real
    part and poles imaginary part, flattened in an array
    (params = np.append(f, np.array([fn, re, im]).
    """
    return 20*np.log10(k*np.abs(_model(params[0:-3],
                                       params[-3],
                                       params[-2],
                                       params[-1])))


def _csvfile2arrays(file):
    """
    Read quartiles csv file as computed by crosscomparison_ir.
    """
    my_csv = pandas.read_csv(file, sep=',', header=0)
    return (np.array(my_csv.frequency),
            np.array(my_csv.amplitude_25),
            np.array(my_csv.amplitude_50),
            np.array(my_csv.amplitude_75),
            np.array(my_csv.phase_25),
            np.array(my_csv.phase_50),
            np.array(my_csv.phase_75))


def _plot_bode(fig, f, fn, fc, h, k, lab):
    """
    Bode plot from an amplitude and a phase.
    """
    H = k*_model(f, fn, fc, h)
    amp = 20*np.log10(np.abs(H))
    pha = np.angle(H, deg=True)
    fig.axes[0].semilogx(f, amp, label=lab)
    fig.axes[1].semilogx(f, pha)


def calc_optimal_pz(f, fnorm, amp, pha):
    """
    Performs the curve_fit, first from the phase to get best poles, then from
    amplitude to get sensitivity.
    """
    params = np.append(f, fnorm)
    if _is_polarity_inverted(f, pha):
        popt, _ = curve_fit(_model_pha, params, pha+180)
    elif not _is_polarity_inverted(f, pha):
        popt, _ = curve_fit(_model_pha, params, pha)
    params = np.append(f, np.array([fnorm, popt[0], popt[1]]))
    kopt, _ = curve_fit(_model_amp, params, amp)
    return popt[0], popt[1], kopt


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot on a same figure the \
                                     median instrumental response measured, \
                                     and its best fit')
    parser.add_argument('input_file', help='path of the csv file containing \
                        the quartiles transfer function')
    parser.add_argument('--fmin', help='Calculate pz for frequencies \
                        contained between fmin and fmax.',
                        type=float, default=0.1)
    parser.add_argument('--fmax', help='Calculate pz for frequencies \
                        contained between fmin and fmax.',
                        type=float, default=10)
    parser.add_argument('--fnorm', help='Calculate the sensitivity at that \
                        frequency',
                        type=float, default=50)
    args = parser.parse_args()

    (f,
     amp25, amp50, amp75,
     pha25, pha50, pha75) = _csvfile2arrays(args.input_file)

    imin = np.abs(f-args.fmin).argmin()
    imax = np.abs(f-args.fmax).argmin()

    # 25th percentile
    fc25, h25, k25 = calc_optimal_pz(f[imin:imax],
                                     args.fnorm,
                                     amp25[imin:imax],
                                     pha25[imin:imax])

    # 75th percentile
    fc75, h75, k75 = calc_optimal_pz(f[imin:imax],
                                     args.fnorm,
                                     amp75[imin:imax],
                                     pha75[imin:imax])

    # 50th percentile (median)
    fc, h, k = calc_optimal_pz(f[imin:imax],
                               args.fnorm,
                               amp50[imin:imax],
                               pha50[imin:imax])

    if _is_polarity_inverted(f, pha50):
        k *= -1

    print("Best fit computed between %.1f and %.1f Hz" % (args.fmin,
                                                          args.fmax))
    print("Sensitivity of sensor: %d counts*s/m" % k)
    print("    Interquartile distance: %d counts*s/m" % (np.abs(k75-k25)))
    print("Cutoff frequency: %.3f Hz" % fc)
    print("    Interquartile distance: %.3f Hz" % (np.abs(fc75-fc25)))
    print("Damping: %.6f" % h)
    print("    Interquartile distance: %.6f" % (np.abs(h75-h25)))
    (zeros, poles) = _calc_pz(fc, h)
    print("Zeros:")
    print(zeros)
    print("Poles:")
    print(poles)
    print("Normalization factor: %f at %d Hz" % (_calc_norm_factor(zeros,
                                                                   poles,
                                                                   args.fnorm),
                                                 args.fnorm))

    fig = pyplot.figure(dpi=150)
    fig.suptitle("Transfer function best fit")
    fig.add_subplot(211)
    fig.add_subplot(212)
    fig.axes[0].semilogx(f, amp50, label='median')
    fig.axes[1].semilogx(f, pha50)
    _plot_bode(fig, f, args.fnorm, fc, h, k, 'median')
    fig.axes[0].legend()
    fig.axes[0].set_ylabel('Amplitude [dB][m/s]')
    fig.axes[1].set_ylabel('Phase [deg]')
    fig.axes[1].set_xlabel('Frequency [Hz]')
    [ax.grid() for ax in fig.axes]
    pyplot.show()
