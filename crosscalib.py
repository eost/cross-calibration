import argparse
from copy import deepcopy
import csv
import os

from matplotlib import pyplot
import numpy as np
from obspy import Stream, UTCDateTime, read
from obspy.clients.fdsn import Client
from scipy.signal import freqs_zpk
import yaml


def read_yaml(path_conf):
    """
    Read the configuration file from the path and return the content.
    """
    with open(path_conf) as conf_yaml:
        conf = yaml.load(conf_yaml, Loader=yaml.SafeLoader)
    return conf


def str2nbr(str_elem):
    """
    Convert a str in float or in complex number
    """
    if 'j' in str_elem:
        return complex(str_elem)
    elif 'e' in str_elem:
        return float(str_elem)
    else:
        raise ValueError(str_elem)


def parse_paz_from_conf(paz):
    """
    Convert in the good format(complex or float) the poles, zeros,
    and normalization frequency of the sensor.
    """
    dict_prop = {}
    for prop_name, prop_val in paz.items():
        if isinstance(prop_val, list):
            list_conv = []
            for elem in prop_val:
                if isinstance(elem, str):
                    elem_conv = str2nbr(elem)
                elif isinstance(elem, float) or isinstance(elem, int):
                    elem_conv = elem
                else:
                    raise ValueError(elem)
                list_conv.append(elem_conv)
            dict_prop[prop_name] = list_conv
        elif isinstance(prop_val, str):
            prop_val = str2nbr(prop_val)
            dict_prop[prop_name] = prop_val
        else:
            dict_prop[prop_name] = prop_val
    return dict_prop


def tf_from_fdsn(trace, url_server, freq):
    """
    Return a dictionnary named paz containing
    the poles, zeros, normalization factor of the system
    having recorded the trace using the FDSN service. If the station dataless
    is not present on the FDSN, the user has to fill the paz himself.
    """
    client = Client(url_server)
    inventory = client.get_stations(starttime=trace.stats.starttime,
                                    endtime=trace.stats.endtime,
                                    network=trace.stats.network,
                                    sta=trace.stats.station,
                                    loc=trace.stats.location,
                                    channel=trace.stats.channel,
                                    level='response')
    seed_id = '{}.{}.{}.{}'.format(trace.stats.network,
                                   trace.stats.station,
                                   trace.stats.location,
                                   trace.stats.channel)
    response = inventory.get_response(seed_id, trace.stats.starttime)
    tf = response.get_evalresp_response_for_frequencies(freq)
    if inventory.get_channel_metadata(seed_id)['dip'] == 90 or \
       inventory.get_channel_metadata(seed_id)['azimuth'] == 180 or \
       inventory.get_channel_metadata(seed_id)['azimuth'] == 270:
        tf *= -1
    return tf


def cmp_normalization_factor(zeros, poles, fn):
    """
    Computes the normalization factor from the zeros, the poles and the
    normalization frequency.
    """
    num = np.prod(2j*np.pi*fn-poles)
    denom = np.prod(2j * np.pi * fn-zeros)
    normalization_factor = np.absolute(num/denom)
    return normalization_factor


def extract_tf(paz_from_conf, conf, trace, key, freq):
    """
    Extract paz of the sensor.
    """
    if paz_from_conf:
        paz = parse_paz_from_conf(conf['paz'][key])
        z = np.array(paz['zeros'])
        p = np.array(paz['poles'])
        fn = np.array(paz['normalization_frequency'])
        k = cmp_normalization_factor(z, p, fn)
        g = float(paz['sensitivity'])
        _, tf = freqs_zpk(z, p, k*g, worN=2*np.pi*freq)
    else:
        tf = tf_from_fdsn(trace, conf['param_extract']['url_server'], freq)
    return tf


def extract_stream(conf_extr, path_reference, path_test):
    """
    Return the reference stream and the test stream.
    Two possibilities to obtain the streams: from the FDSN service or
    directly by the stream path given by the user in input of the programme
    """
    if path_reference is None and path_test is None:
        url_server = conf_extr['url_server']
        start_time = UTCDateTime(conf_extr['starttime'])
        end_time = UTCDateTime(conf_extr['endtime'])
        client = Client(url_server)
        station_reference = conf_extr['station_reference'].replace('__', '')
        print("Start waveform extraction: %s" % station_reference)
        stream_reference = client.get_waveforms(*station_reference.split('.'),
                                                start_time, end_time)
        station_test = conf_extr['station_test'].replace('__', '')
        print("Start waveform extraction: %s" % station_test)
        stream_test = client.get_waveforms(*station_test.split('.'),
                                           start_time, end_time)
    else:
        stream_reference = read(path_reference)
        stream_test = read(path_test)
    return stream_reference, stream_test


def stream2trace(stream):
    """
    Check that the stream in input contains only one trace and return
    the corresponding trace. If the stream is composed of multiple trace,
    it raises a ValueError.
    """
    if len(stream) == 1:
        tr = stream[0]
        return tr
    else:
        msg = "Stream got multiple traces!"
        raise ValueError(msg)


def slice_traces(tr_ref, tr_test, conf):
    """
    Slice the two traces in order to have two traces starting and finishing
    at the same time.
    """
    st_ref = Stream(tr_ref)
    st_test = Stream(tr_test)
    starttime = UTCDateTime(conf['param_extract']['starttime'])
    endtime = UTCDateTime(conf['param_extract']['endtime'])
    st_ref = st_ref.slice(starttime, endtime)
    st_test = st_test.slice(starttime, endtime)
    return st_ref[0], st_test[0]


def start_end_correct(tr_ref, tr_test):
    """
    Return True if the starttime and the endtime is equal
    (tolerate time difference of one sample). Else return False.
    """
    time_equality = False
    time_tolerance = max(tr_ref.stats.delta, tr_test.stats.delta)
    if abs(tr_ref.stats.starttime - tr_test.stats.starttime) <= time_tolerance\
       and abs(tr_ref.stats.endtime - tr_test.stats.endtime) <= time_tolerance:
        time_equality = True
    return time_equality


def check_time_traces(tr_ref, tr_test, path_reference, path_test, conf):
    """
    If traces obtained with FDSN, slice traces to have traces starting
    and finishing at the same time.
    If traces come from mseed file given by the user,
    check that the starttime and the endtime are the same for the two traces.
    """
    if path_reference is None and path_test is None:
        tr_ref, tr_test = slice_traces(tr_ref, tr_test, conf)
        if not start_end_correct(tr_ref, tr_test):
            msg = 'starttime and endtime is not equal between the two traces.\
                   It may be due the unavailability of the data of one of the \
                   sensors.'
            raise Exception(msg)
    else:
        if not start_end_correct(tr_ref, tr_test):
            msg = 'starttime and endtime is not equal between the two traces'
            raise Exception(msg)
    return tr_ref, tr_test


def segment_trace(trace, length_segment, overlap):
    """
    The trace is segmented in N pieces.
    Each piece has a length equal to length_segment and overlap with
    the previous piece.
    Return a list containing N segments of the trace.
    """
    nbr_sample_segment = int(trace.stats.sampling_rate*60*length_segment)
    nbr_sample_overlap = int(overlap * nbr_sample_segment)
    if nbr_sample_segment > trace.stats.npts:
        msg = "parameter segment_length is too high"
        raise ValueError(msg)
    else:
        ind_deb, ind_end = (0, nbr_sample_segment)
        trace_segmented = []
        while ind_deb <= trace.stats.npts - nbr_sample_segment:
            segment = deepcopy(trace)
            segment_data = trace.data[ind_deb:ind_end]
            segment.data = segment_data
            trace_segmented.append(segment)
            ind_deb = ind_end - nbr_sample_overlap
            ind_end = ind_deb + nbr_sample_segment
        return trace_segmented


def frac_octave_smooth(n, freq, data):
    """
    Performs a fractional 1/n octave smooth on data
    """
    if n > 0:
        for f in freq:
            nth_root = 2**(1/n)
            lowerf = f/nth_root
            upperf = f*nth_root
            ind = [np.abs(freq-bound).argmin()
                   for bound in sorted([lowerf, f, upperf])]
            if ind[0] == ind[2]:
                continue
            else:
                data[ind[1]] = data[ind[0]:ind[2]].mean()
    return data


def cmp_common_frequency_vector(segment_ref, segment_test):
    """
    Compute the common frequency vector betwenn the two traces.
    The maximum frequency is linked to the lowest sampling rate of
    the two traces. The frequence difference between neighbor frequencies
    is equal to the inverse of the duration of the two traces.
    """
    nyquist_frequency_ref = segment_ref.stats.sampling_rate / 2
    nyquist_frequency_test = segment_test.stats.sampling_rate / 2
    if nyquist_frequency_ref <= nyquist_frequency_test:
        freq = np.fft.rfftfreq(segment_ref.stats.npts, segment_ref.stats.delta)
    else:
        freq = np.fft.rfftfreq(segment_test.stats.npts,
                               segment_test.stats.delta)
    return freq[1:]


def compute_spectrum(trace_segmented, freq, conf):
    """
    Compute the smoothed spectrum of each segment of the trace segmented.
    Return a list, each element of the list is the fft of a trace segment.
    """
    perc_tap = conf['spectrum']['max_percentage']
    fft_trace_segmented = []
    for seg in trace_segmented:
        seg.taper(perc_tap, beta=4*np.pi, type='kaiser')
        fft_seg = np.fft.rfft(seg.data) * seg.stats.delta
        fft_trace_segmented.append(fft_seg[1:len(freq) + 1])
    return fft_trace_segmented


def compute_tf(tr_reference, tr_test, conf, paz_ref_from_conf):
    """
    Compute the transfer function of the tested system
    using the method described by Pavlis
    (Pavlis, 1994, calibration of seismometers using ground noise).
    """
    length_segment = conf['spectrum']['segment_length']
    perc_overlap = conf['spectrum']['percentage_overlapping']
    [tr.detrend('demean') for tr in [tr_reference, tr_test]]
    reference_segmented = segment_trace(tr_reference,
                                        length_segment,
                                        perc_overlap)
    test_segmented = segment_trace(tr_test,
                                   length_segment,
                                   perc_overlap)
    freq = cmp_common_frequency_vector(reference_segmented[0],
                                       test_segmented[0])
    fft_ref_seg = compute_spectrum(reference_segmented, freq, conf)
    fft_test_seg = compute_spectrum(test_segmented, freq, conf)
    tf_ref = extract_tf(paz_ref_from_conf, conf,
                        tr_reference, 'reference', freq)
    tf_test_raw = []
    for i in range(0, len(fft_ref_seg)):
        ratio_spectrum = np.divide(fft_test_seg[i], fft_ref_seg[i])
        tf_seg = np.multiply(ratio_spectrum, tf_ref)
        tf_test_raw.append(tf_seg)
    tf_test_raw = np.asarray(tf_test_raw)
    tf_test_raw = np.transpose(tf_test_raw)
    return tf_test_raw, freq


def plot_tf(path_figure, tf_theoretical, amplitude_tf_exp, phase_tf_exp, freq):
    """
    Plot the smoothed amplitude spectrum and phase spectrum
    of the experimental and theoretical transfer function.
    """
    amp_spectrum_th = 20 * np.log10(np.abs(tf_theoretical))
    phase_spectrum_th = np.angle(tf_theoretical, deg=True)
    fig = pyplot.figure()
    ax1 = fig.add_subplot(211)
    ax1.semilogx(freq, amplitude_tf_exp, linewidth=2,
                 color='blue', label='measured')
    ax1.semilogx(freq, amp_spectrum_th, linewidth=2,
                 color='green', label='theory')
    ax1.set_xlim(freq[0], freq[-1])
    ax1.grid()
    ax1.legend(loc='best')
    prop_text = {'weight': 'bold', 'size': 'large'}
    ax1.set_ylabel('Amplitude [dB]', prop_text)
    ax2 = fig.add_subplot(212)
    ax2.semilogx(freq, phase_tf_exp, linewidth=2,
                 color='blue')
    ax2.semilogx(freq, phase_spectrum_th, linewidth=2,
                 color='green')
    ax2.grid()
    ax2.set_xlim(freq[0], freq[-1])
    ax2.set_ylim(-180, 180)
    ax2.set_ylabel('Phase [deg]', prop_text)
    ax2.set_xlabel('Frequency [Hz]', prop_text)
    pyplot.savefig(path_figure, dpi=300)


def write_tf_csv(amplitude, phase, frequency, path_csv):
    """
    Write in a csv file the frequency, amplitude (dB) and phase (degree)
    of the experimental transfer function computed.
    """
    with open(path_csv, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=['frequency',
                                'amplitude', 'phase'])
        writer.writeheader()
        for i in range(0, amplitude.size):
            writer.writerow({'frequency': round(frequency[i], 4),
                             'amplitude': round(amplitude[i], 1),
                             'phase': round(phase[i], 1)})


def crosscalib(path_conf, rep_output, paz_ref_from_conf, paz_test_from_conf,
               path_reference_stream, path_test_stream):
    """
    Execute all the steps to obtain the figure of the measured instrumental
    response.
    """
    conf = read_yaml(path_conf)
    stream_ref, stream_test = extract_stream(conf['param_extract'],
                                             path_reference_stream,
                                             path_test_stream)
    tr_ref, tr_test = (stream2trace(stream_ref), stream2trace(stream_test))
    tr_ref, tr_test = check_time_traces(tr_ref, tr_test, path_reference_stream,
                                        path_test_stream, conf)
    tf_test_raw, freq = compute_tf(tr_ref, tr_test, conf, paz_ref_from_conf)
    tf_test_exp = np.median(tf_test_raw, axis=1)
    smoothie = conf['spectrum']['smoothie']
    tf_test_exp = frac_octave_smooth(smoothie, freq, tf_test_exp)
    amplitude_tf_test_exp = 20 * np.log10(np.abs(tf_test_exp))
    phase_tf_test_exp = np.angle(tf_test_exp, deg=True)
    tf_test_theoretical = extract_tf(paz_test_from_conf, conf, tr_test,
                                     'test', freq)
    figure_name = "IR_{0}.png".format(tr_test.id)
    path_figure = os.path.join(rep_output, figure_name)
    plot_tf(path_figure, tf_test_theoretical, amplitude_tf_test_exp,
            phase_tf_test_exp, freq)
    path_csv = path_figure.replace('.png', '.csv')
    write_tf_csv(amplitude_tf_test_exp, phase_tf_test_exp, freq, path_csv)
    return amplitude_tf_test_exp, phase_tf_test_exp, freq


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot the transfer\
                                     function of a tested system from\
                                     a reference system located closely')
    parser.add_argument('conf_file', help='path configuration file\
                        in yaml format')
    parser.add_argument('rep_output', help='directory where the plot of the\
                        measured instrumental response will be saved')
    parser.add_argument('-r', '--reference_stream', help='path of the mseed\
                        file containing the stream of the reference system')
    parser.add_argument('-t', '--test_stream', help='path of the mseed\
                        file containing the stream of the test system')
    parser.add_argument('-i', '--paz_ref_from_conf', help='read paz of the\
                        reference sensor from the configuration file.',
                        action='store_true')
    parser.add_argument('-j', '--paz_test_from_conf', help='read paz of the\
                        test sensor from the configuration file',
                        action='store_true')
    args = parser.parse_args()
    path_conf = args.conf_file
    rep_output = args.rep_output
    path_reference = args.reference_stream
    path_test = args.test_stream
    paz_ref_from_conf = args.paz_ref_from_conf
    paz_test_from_conf = args.paz_test_from_conf
    crosscalib(path_conf, rep_output, paz_ref_from_conf, paz_test_from_conf,
               path_reference, path_test)
