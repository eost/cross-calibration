import argparse
from collections import defaultdict
import csv
import os
import pandas as pd
import yaml

from matplotlib import pyplot
import numpy as np
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
from scipy.signal import freqs_zpk


def load_ir_csv(rep_input):
    """
    Return the frequency vector and a dictionnary containing the amplitude
    and the phase of the instrumental responses of the stations.
    """
    data_loaded = defaultdict(dict)
    for sta_dir in os.listdir(rep_input):
        path_sta_dir = os.path.join(rep_input, sta_dir)
        for file in os.listdir(path_sta_dir):
            if file.endswith('.csv'):
                path_csv = os.path.join(path_sta_dir, file)
                my_csv = pd.read_csv(path_csv, sep=',', header=0)
                data_loaded[sta_dir]['amp'] = my_csv.amplitude
                data_loaded[sta_dir]['phase'] = my_csv.phase
    freq = my_csv.frequency
    return freq, data_loaded


def cmp_percentile(ir_stations, n):
    """
    Return the n-th percentile of amplitude and phase
    of all the instrumental responses.
    """
    amplitudes = []
    phases = []
    for sta in ir_stations.keys():
        amplitudes.append(ir_stations[sta]['amp'])
        phases.append(ir_stations[sta]['phase'])
    amplitude_percentile = np.percentile(amplitudes, n, axis=0)
    phase_percentile = np.percentile(phases, n, axis=0)
    return amplitude_percentile, phase_percentile


def read_yaml(path_conf):
    """
    Read the configuration file from the path and return the content.
    """
    with open(path_conf) as conf_yaml:
        conf = yaml.load(conf_yaml, Loader=yaml.SafeLoader)
    return conf


def str2nbr(str_elem):
    """
    Convert a str in float or in complex number
    """
    if 'j' in str_elem:
        return complex(str_elem)
    elif 'e' in str_elem:
        return float(str_elem)
    else:
        raise ValueError(str_elem)


def parse_paz_from_conf(paz):
    """
    Convert in the good format(complex or float) the poles, zeros,
    and normalization frequency of the sensor.
    """
    dict_prop = {}
    for prop_name, prop_val in paz.items():
        if isinstance(prop_val, list):
            list_conv = []
            for elem in prop_val:
                if isinstance(elem, str):
                    elem_conv = str2nbr(elem)
                elif isinstance(elem, float) or isinstance(elem, int):
                    elem_conv = elem
                else:
                    raise ValueError(elem)
                list_conv.append(elem_conv)
            dict_prop[prop_name] = list_conv
        elif isinstance(prop_val, str):
            prop_val = str2nbr(prop_val)
            dict_prop[prop_name] = prop_val
        else:
            dict_prop[prop_name] = prop_val
    return dict_prop


def cmp_normalization_factor(paz):
    """
    Computes the normalization factor from the zeros, the poles and the
    normalization frequency.
    """
    zeros = np.array(paz['zeros'])
    poles = np.array(paz['poles'])
    fn = np.array(paz['normalization_frequency'])
    num = np.prod(2j*np.pi*fn-poles)
    denom = np.prod(2j * np.pi * fn-zeros)
    normalization_factor = np.absolute(num/denom)
    return normalization_factor


def tf_from_fdsn(conf, freq):
    """
    Return a dictionnary named paz containing
    the poles, zeros, normalization factor of the system
    having recorded the trace using the FDSN service. If the station dataless
    is not present on the FDSN, the user has to fill the paz himself.
    """
    client = Client(conf['param_extract']['url_server'])
    t1 = UTCDateTime(conf['param_extract']['starttime'])
    t2 = UTCDateTime(conf['param_extract']['endtime'])
    seed_id = conf['param_extract']['station_test']
    net, sta, loc, chan = seed_id.split('.')
    inventory = client.get_stations(starttime=t1,
                                    endtime=t2,
                                    network=net,
                                    sta=sta,
                                    loc=loc,
                                    channel=chan,
                                    level='response')
    chan = inventory[0][0][0]
    if chan.sensor.type is not None and chan.sensor.type.lower() == 'generic':
        raise Exception
    if chan.data_logger.type is not None and \
       chan.data_logger.type.lower() == 'generic':
        raise Exception
    response = inventory.get_response(seed_id, t1)
    tf = response.get_evalresp_response_for_frequencies(freq)
    if inventory.get_channel_metadata(seed_id)['dip'] == 90 or \
       inventory.get_channel_metadata(seed_id)['azimuth'] == 180 or \
       inventory.get_channel_metadata(seed_id)['azimuth'] == 270:
        tf *= -1
    return tf


def read_theoretical_tf(rep_input, freq):
    """
    Return the theoretical instrumental response of the tested system.
    """
    # As the theoretical instrumental responses is the same for
    # all the stations  in rep_input, go to the first station directory
    # and try to get the IR from the FDSN otherwise read it in the conf file.
    sta_dir = os.listdir(rep_input)[0]
    path_conf = os.path.join(rep_input, sta_dir, 'conf.yaml')
    conf = read_yaml(path_conf)
    try:
        tf = tf_from_fdsn(conf, freq)
        print("Read response from FDSN")
    except Exception:
        paz = parse_paz_from_conf(conf['paz']['test'])
        paz['normalization_factor'] = cmp_normalization_factor(paz)
        print("Read PAZ from the configuration file")
        z, p, k = (paz['zeros'], paz['poles'],
                   paz['normalization_factor']*paz['sensitivity'])
        _, tf = freqs_zpk(z, p, k, worN=2*np.pi*freq)
    return tf


def plot_amplitude_spectrum(ir_stations, amp_50, th_amp, f_min,
                            freq, rep_output):
    """
    Plot the amplitude spectrum of all the instrumental responses, hold on
    the median and the theoretical curve.
    """
    sta_nbr = len(ir_stations.keys())
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    prop_text = {'weight': 'bold', 'size': 'large'}
    ax.set_ylabel('Amplitude [dB]', prop_text)
    ax.set_xlabel('Frequency [Hz]', prop_text)
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, sta_nbr + 1, dtype=np.int16)
    ind = 0
    ind_fmin = np.abs(np.array(freq)-f_min).argmin()
    for sta in ir_stations.keys():
        amp = ir_stations[sta]['amp']
        ax.semilogx(freq[ind_fmin:], amp[ind_fmin:], linewidth=1,
                    color=cmap(index_color[ind]), alpha=0.4)
        ind += 1
    ax.grid()
    line_amp_50, = ax.semilogx(freq[ind_fmin:], amp_50[ind_fmin:], linewidth=1,
                               color=cmap(index_color[-1]))
    line_th_amp, = ax.semilogx(freq[ind_fmin:], th_amp[ind_fmin:], color='k')
    ax.legend([line_amp_50, line_th_amp], ['median', 'theory'], loc='best')
    path_figure = os.path.join(rep_output, 'cross_amplitude_spectrum.png')
    pyplot.savefig(path_figure, dpi=300)
    pyplot.cla()
    pyplot.clf()


def plot_phase_spectrum(ir_stations, phase_50, th_phase,  f_min,
                        freq, rep_output):
    """
    Plot the phase spectrum of all the instrumental responses, hold on
    the median and the theoretical curve.
    """
    sta_nbr = len(ir_stations.keys())
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    prop_text = {'weight': 'bold', 'size': 'large'}
    ax.set_ylabel('Phase [deg]', prop_text)
    ax.set_xlabel('Frequency [Hz]', prop_text)
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, sta_nbr + 1, dtype=np.int16)
    ind = 0
    ind_fmin = np.abs(np.array(freq)-f_min).argmin()
    for sta in ir_stations.keys():
        phase = ir_stations[sta]['phase']
        ax.semilogx(freq[ind_fmin:], phase[ind_fmin:], linewidth=1,
                    color=cmap(index_color[ind]), alpha=0.4)
        ind += 1
    ax.grid()
    line_phase_50, = ax.semilogx(freq[ind_fmin:], phase_50[ind_fmin:],
                                 linewidth=1, color=cmap(index_color[-1]),
                                 label='median')
    line_phase_th, = ax.semilogx(freq[ind_fmin:], th_phase[ind_fmin:],
                                 color='k', label='theory')
    ax.legend([line_phase_50, line_phase_th], ['median', 'theory'], loc='best')
    path_figure = os.path.join(rep_output, 'cross_phase_spectrum.png')
    pyplot.savefig(path_figure, dpi=300)
    pyplot.cla()
    pyplot.clf()


def write_quartiles(amp25, amp50, amp75, pha25, pha50, pha75, freq,
                    rep_output):
    """
    Write in a csv file the median amplitude and the median phase
    of the instrumental responses. The csv file will
    be saved in the directory named rep_output.
    """
    path_csv = os.path.join(rep_output, 'quartiles_ir.csv')
    with open(path_csv, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=['frequency',
                                                      'amplitude_25',
                                                      'amplitude_50',
                                                      'amplitude_75',
                                                      'phase_25',
                                                      'phase_50',
                                                      'phase_75'])
        writer.writeheader()
        for i in range(0, amp_50.size):
            writer.writerow({'frequency': freq[i],
                             'amplitude_25': amp25[i],
                             'amplitude_50': amp50[i],
                             'amplitude_75': amp75[i],
                             'phase_25': pha25[i],
                             'phase_50': pha50[i],
                             'phase_75': pha75[i]})


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot on a same figure the \
                                     different instrumental response measured')
    parser.add_argument('rep_input', help='path of the repertory containing \
                        the stations directories. In these stations \
                        directories, there is a csv file  containing \
                        the instrumental response')
    parser.add_argument('rep_output', help='repertory where the results will\
                         be outputted')
    parser.add_argument('-f', '--fmin', help='Plot the instrumental response\
                        for frequencies contained between fmin and the \
                        Nyquist frequency.', type=float, default=0.1)
    args = parser.parse_args()
    rep_input = args.rep_input
    rep_output = args.rep_output
    f_min = args.fmin
    freq, ir_stations = load_ir_csv(rep_input)
    amp_50, phase_50 = cmp_percentile(ir_stations, 50)
    amp_25, phase_25 = cmp_percentile(ir_stations, 25)
    amp_75, phase_75 = cmp_percentile(ir_stations, 75)
    th_tf = read_theoretical_tf(rep_input, freq)
    plot_amplitude_spectrum(ir_stations, amp_50, 20*np.log10(np.abs(th_tf)),
                            f_min, freq, rep_output)
    plot_phase_spectrum(ir_stations, phase_50, np.angle(th_tf, deg=True),
                        f_min, freq, rep_output)
    write_quartiles(amp_25, amp_50, amp_75,
                    phase_25, phase_50, phase_75,
                    freq, rep_output)
