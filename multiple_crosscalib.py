import argparse
import csv
import os
import yaml

from crosscalib import crosscalib, read_yaml


def make_stations_id(path_csv):
    """
    Return a list, each element of the list is a station id.
    """
    with open(path_csv, 'r') as csv_file:
        csv_content = csv.DictReader(csv_file, delimiter=',')
        stations_id = []
        for row in csv_content:
            if row['location_code'] == '__':
                row['location_code'] = ''
            station_id = "{0}.{1}.{2}.{3}".format(row['network'],
                                                  row['station'],
                                                  row['location_code'],
                                                  row['channel'])
            stations_id.append(station_id)
    return stations_id


def write_yaml(data, path_doc):
    """
    Write the data in a doc
    """
    with open(path_doc, 'w') as conf_yaml:
        yaml.dump(data, stream=conf_yaml, default_flow_style=False)


def process(path_conf, sta_list, path_result,
            paz_ref_from_conf, paz_test_from_conf):
    """
    For each station, it creates a directory in path_result
    where the file configuration and the figure will be saved.
    """
    conf = read_yaml(path_conf)
    for sta in sta_list:
        path_rep_sta_id = os.path.join(path_result, sta)
        try:
            os.mkdir(path_rep_sta_id)
        except FileExistsError:
            pass
        conf['param_extract']['station_test'] = sta
        path_conf_updated = os.path.join(path_rep_sta_id, 'conf.yaml')
        if len(os.listdir(path_rep_sta_id)) < 3:
            write_yaml(conf, path_conf_updated)
            crosscalib(path_conf_updated, path_rep_sta_id, paz_ref_from_conf,
                       paz_test_from_conf, None, None)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute the module crosscalib\
                                     (measure the instrumental system of a \
                                     test system) for several test systems\
                                     having the same model type.\
                                     The tested system are all the stations\
                                     between first_station and last_station')
    parser.add_argument('conf_file', help='path configuration file\
                        in yaml format')
    parser.add_argument('rep_output', help='directory where the plot of the\
                        measured instrumental response will be saved')
    parser.add_argument('csv_stations', help='path csv file containing the\
                        stations id')
    parser.add_argument('-i', '--paz_ref_from_conf', help='read paz of the\
                        reference sensor from the configuration file.',
                        action='store_true')
    parser.add_argument('-j', '--paz_test_from_conf', help='read paz of the\
                        test sensor from the configuration file',
                        action='store_true')
    args = parser.parse_args()
    path_conf = args.conf_file
    rep_output = args.rep_output
    csv_stations = args.csv_stations
    paz_ref_from_conf = args.paz_ref_from_conf
    paz_test_from_conf = args.paz_test_from_conf
    rep_output = os.path.join(rep_output, 'crosscalib')
    try:
        os.mkdir(rep_output)
    except FileExistsError:
        pass
    all_tested_system = make_stations_id(csv_stations)
    process(path_conf, all_tested_system, rep_output,
            paz_ref_from_conf, paz_test_from_conf)
